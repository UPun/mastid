/**
 * Created by urmasp on 2.02.2017.
 */
'use strict';

function Mastid(){
    var kuva = document.getElementById("tulemus-div");
    var button = document.getElementById("b1");
    kuva.style.display = 'block';

    var erinevadMastid = new Array();
    var erinevadMastid = ["ärtu", "ruutu", "risti", "poti"];

    button.onclick = function () {
        var juhuslikMast = erinevadMastid[Math.floor((Math.random() * 3) + 0)];
        // console.log(juhuslikMast);

        var varvus = undefined;

        if (juhuslikMast == 'risti'){
            juhuslikMast = 'Risti ';
            varvus = "black";       //must
            risti()
            var kutsuVälja = risti()
        }
        if (juhuslikMast == "ruutu"){
            juhuslikMast = "Ruutu ";
            varvus = "#ff0000";     //punane
            ruutu()
            var kutsuVälja = ruutu()
        }
        if (juhuslikMast == "poti"){
            juhuslikMast = "Poti ";
            varvus = "black";       //must
            poti()
            var kutsuVälja = poti()
        }
        if (juhuslikMast == "ärtu"){
            juhuslikMast = "Ärtu ";
            varvus = "#ff0000";     //punane
            ärtu()
            var kutsuVälja = ärtu()
        }
        kuva.innerHTML = juhuslikMast;

        // console.log(juhuslikMast)

        myFunction(varvus)

    }
}

function ärtu(){
    console.log("ärtu")
    console.log("♥")
}

function ruutu(){
    console.log("ruutu")
    console.log("♦")
}

function risti(){
    console.log("risti")
    console.log("♣")
}

function poti(){
    console.log("poti")
    console.log("♠")
}

function myFunction(varvus) {
    document.getElementById("tulemus-div").style.color = varvus;
}